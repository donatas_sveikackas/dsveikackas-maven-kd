package lt.vtvpmc.Sveikackas;

import java.util.UUID;

public class App {

	public static void main(String[] args) {
		App obj = new App();
		System.out.println("Unique ID : " + obj.generateUniqueKey()); // Isvestis
	}

	public String generateUniqueKey() {

		String id = UUID.randomUUID().toString(); // Generuoja NR
		return id;

	}
}